import sqlite3
import time
import datetime
class DBitem():
    def __init__(self, data, rowid, column, table):
        self.rowid=rowid
        self.data=data
        self.table=table
        self.column=column
class DataBase():
    def __init__(self):
        #This procedure sets up a database connection
        self.conn=sqlite3.connect('johnnybase.db')
        self.c = self.conn.cursor()
            
    def setup(self):
        #create databases
        self.c.execute('''CREATE TABLE settings (name text, intvalue int, textvalue text, type int) ''')
        #Setting types
        #1 - binary
        #2 - integer
        #3 - interval
        #4 - text
        self.c.execute('''CREATE TABLE alarms (timestamp int) ''')
        self.c.execute('''CREATE TABLE jokes (lastheard int, joke text) ''')
    
        query = (
            ('Volume','80', '0-100', '3'), # Volume
            ('Snooze minutes','5', '', '2'), # Time (mins) to snooze for
            ('Max. brightness','15', '0-100', '3'), # Maximum brightness
            ('Min brightness','1', '0-100', '3'), # Minimum brightness
            ('Display timeout','20', '', '2'), # Time (secs) after which we should revert to auto-brightness
            ('Menu timeout','20', '', '2'), # Time (secs) after which an un-touched menu should close
            ('Alarm timeout','120', '', '2'), # If the alarm is still going off after this many seconds, stop it
            ('Holiday mode on/off','0', '', '1'), # Is holiday mode (no auto-alarm setting) enabled?
            ('Sound on/off','1', '', '1'), # Are sound effects enabled?
            ('Weather_on_alarm','0', '', '1'), # Read out the weather on alarm cancel
            ('Joke on Alarm','0', '', '1'), # Read out the weather on alarm cancel
            ('Preempt alarm seconds','600', '', '2'), # Number of seconds before an alarm that we're allowed to cancel it
            ('Location','Essen, Germany', '', '4'), # Location for home
            ('Google Login','', '', '4'), # Location for home
            ('Google Password','', '', '4'), # Location for home
            ('Google Calendar Address','', '', '4'), # Location for home
        )   
        for row in query:
            self.c.execute('''INSERT INTO settings (name, intvalue, textvalue, type) VALUES (?, ?, ?, ?)''', row)

    def getsettings(self):
        #This downloads and returns a dictionary of settings
        settings = dict()
        for row in self.c.execute('''SELECT * FROM settings''') :
            self.settings[row[0]] = row[1]
        return settings

    def write(self, dbitem):
        for datum in dbitem.data:
            query= (dbitem.table, datum[0], datum[1], dbitem.rowid)
            self.c.execute('''UPDATE ? SET ? = ? WHERE ROWID = ? ''', query)

    def writesetting(self, name, value):
        #Takes a tuple of value and name and writes it to the database
        query = (value, name)
        self.c.execute('''UPDATE settings SET value = ? WHERE name = ? ''', query)

    def addjoke(self, joke):
        #Takes a joke and inserts it into the database
        today = time.mktime(datetime.datetime.now())
        query = (joke, today)
        self.c.execute('''INSERT INTO jokes VALUES (?, ?) ''', query)

    def getjoke(self, joke):
        #Gets a joke from the database and returns it's text
        query = (joke,)
        self.c.execute('''SELECT * FROM jokes WHERE ROWID = ? ''', query)
        jokecontent = self.c.fetchone()
        return(jokecontent)

    def deletejoke(self, rowid):
        #Takes a tuple of ROWID and deletes that joke
        query = (rowid,)
        self.c.execute('''DELETE FROM jokes WHERE ROWID = ? ''', query)

    
    def createalarm(self, alarmtime):
        #Takes time and creates an alarm
        query = (alarmtime,)
        self.c.execute('''INSERT INTO alarms SET timestamp = ? WHERE name = ? ''', query)
        
    def getalarms(self):
        #Gets a joke from the database and returns it's text
        self.c.execute('''SELECT * FROM jokes ''')
        alarmlist = self.c.fetchall()
        return(alarmlist)

    def deletealarm(self, rowid):
        #Takes a tuple of ROWID and deletes that joke
        query = (rowid,)
        self.c.execute('''DELETE FROM alarms WHERE ROWID = ? ''', query)
