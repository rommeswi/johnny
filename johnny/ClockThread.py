#!/usr/bin/python
#Johnny runs a clock thread which displays the current time on the LED screen.

import time
import datetime
import pytz
import threading

class ClockThread(threading.Thread):

   def __init__(self):
      threading.Thread.__init__(self)
      self.stopping=False

   def stop(self):
      self.segment.disp.clear()
      self.stopping=True

   def run(self):
      while(not self.stopping):
          now = datetime.datetime.now(pytz.timezone('Europe/London'))
          hour = now.hour

          minute = now.minute
          second = now.second

          self.segment.clear()
          # Set hours
          self.segment.set_digit(0, int(hour / 10))     # Tens
          self.segment.set_digit(1, hour % 10)          # Ones
          self.segment.set_colon(second % 2)
          # Set minutes
          self.segment.set_digit(2, int(minute / 10))   # Tens
          self.segment.set_digit(3, minute % 10)        # Ones
          self.segment.write_display()

          time.sleep(.25)
