#!/usr/bin/python
import logging
logging.basicConfig(filename='/home/pi/johnnylogs/johnnypylog',filemode='w',level=logging.DEBUG)
logging.info('Johnny has been started.')
try:
    import pygame
    from time import sleep
    import datetime
    import threading
    from menu import Menu
    from iomonitor import inputmonitor, outputmonitor    
except ImportError:
    logging.error("Johnny could not find some libraries...")


#In this test version, Johnny will be able to give an audio response if a button is pushed.
#menuitems = self.dbase.getmenuitems(parent)

pygame.mixer.init()
pygame.mixer.music.load("/johnny/johnny/startup.wav")
pygame.mixer.music.play()
while pygame.mixer.music.get_busy() == True:
    sleep(1)

inp=inputmonitor()
out=outputmonitor()

class clockthread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.stopping=False
    def run(self):
        while self.stopping==False:
            self.updateclock()
            sleep(0.1)
    def stop(self):
        self.stopping=True
        out.segment_off()
    def updateclock(self):
        now = datetime.datetime.now()
        hour = now.hour
        minute = now.minute
        second = now.second
        out.segment_show_number(int(hour / 10),hour % 10,int(minute / 10),minute % 10, False, second % 2)

class menuthread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.stopping=False
        self.currentmenuid = 0
        self.menu = Menu(False)
    def run(self):
        while self.stopping==False:
            out.lcd_off()
            inp.wait_for_press()
            msg = self.menu.display()
            out.lcd_lines(msg[0],msg[1],msg[2],msg[3])
            p = inp.wait_for_press_return(20)
            while p!=False:
                if p=="blue":
                    self.menu.home()
                elif p=="white":
                    self.menu.back()
                elif p=="left":
                    self.menu.left()
                elif p=="push":
                    self.menu.select()
                elif p=="right":
                    self.menu.right()
                msg = self.menu.display()
                out.lcd_lines(msg[0],msg[1],msg[2],msg[3])
                p = inp.wait_for_press_return(20)
    def stop(self):
        self.stopping==True


class buttondeamon(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.stopping=False
    def run(self):
        while self.stopping==False:
            self.updateclock()
            self.testlcd()
            if(inp.is_bluebutton_pressed()):
                pygame.mixer.music.load("/johnny/johnny/bluebutton.wav")
                pygame.mixer.music.play()

            if(inp.is_whitebutton_pressed()):
                pygame.mixer.music.load("/johnny/johnny/whitebutton.wav")
                pygame.mixer.music.play()

            if(inp.is_rotarypush_pressed()):
                pygame.mixer.music.load("/johnny/johnny/rotarypush.wav")
                pygame.mixer.music.play()

            if(inp.is_rotaryleft_pressed()):
                pygame.mixer.music.load("/johnny/johnny/rotaryleft.wav")
                pygame.mixer.music.play()

            if(inp.is_rotaryright_pressed()):
                pygame.mixer.music.load("/johnny/johnny/rotaryright.wav")
                pygame.mixer.music.play()
        datetime.time.sleep(.25)
    def stop(self):
        self.stopping=True



mthread = menuthread()
mthread.start()

clock = clockthread()
clock.start()
sleep(30)
clock.stop()