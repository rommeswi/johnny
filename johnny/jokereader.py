import urllib2
import re
from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

class jokeloader ():
    def __init__(self):
        self.jokelist = MLStripper()
    
    def loadjokes(self):
        p = HTMLParser()
        for x in range(62):
            n = (x+1)*10+1
            web = urllib2.urlopen('http://witze-ueber-witze.de/versaute-witze-'+ str(n) + '.html')
            try:
                html = p.unescape(web.read().decode('iso-8859-1'))
                html = re.sub(r'Versaute Sexwitze #([0-9]{,})', '',html)
                jokelist = html.split('<p class="witz">')
                del jokelist[-1]
                del jokelist[0]
                for x in jokelist:
                    self.jokelist.feed(x)
            except:
                print 'Some error happened at ' + str(n)
            print str(n) + ' loaded (Versaut)!'
        for x in range(108):
            n = (x+1)*10+1
            web = urllib2.urlopen('http://witze-ueber-witze.de/arztwitze-'+ str(n) + '.html')
            try:
                html = p.unescape(web.read().decode('iso-8859-1'))
                html = re.sub(r'Arztwitze #([0-9]{,})', '',html)
                jokelist = html.split('<p class="witz">')
                del jokelist[-1]
                del jokelist[0]
                for x in jokelist:
                    self.jokelist.feed(x)
            except:
                print 'Some error happened at ' + str(n)
            print str(n) + ' loaded! (Arzt)'
    def getjoke(self):
        return self.jokelist.get_data()
jl = jokeloader()
jl.loadjokes()