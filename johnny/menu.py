import datetime
import UserList



        
class Menu():
    def __init__(self, dbase):
        self.m=Tree(MenuItem("Menu", 0))
        self.m.append(MenuItem("Submenu1",0))
        self.m.append(MenuItem("Submenu2",0))
        self.m.append(MenuItem("Submenu3",0))
        self.m.down()
        self.m.append(MenuItem("Submenuitem1",0))
        self.m.append(MenuItem("Submenuitem2",0))
        self.m.append(MenuItem("Submenuitem3",0))
        self.m.up()
        self.m.right()
        self.m.down()
        self.m.append(MenuItem("Submenuitema",0))
        self.m.append(MenuItem("Submenuitemb",0))
        self.m.append(MenuItem("Submenuitemc",0))
        self.m.append(MenuItem("Submenuitemd",0))
        self.m.up()
        self.m.right()
        self.m.down()
        self.m.append(MenuItem("Submenuitemi",0))
        self.m.append(MenuItem("Submenuitemii",0))
        self.m.home()
        self.m.right()
        self.dbase = dbase
    def loadmenus(self, parent, menuitems):
        for item in menuitems:
            if item["type"]==0:
                self.m.append(MenuItem(item["rowid"]))
                self.m.down()
                self.loadmenus(item["rowid"])
                self.m.up()
            if item["type"]==1:
                #Binary
                self.m.append(MenuBinary(item["rowid"]))
            if item["type"]==2:
                #List
                self.m.append(MenuList(item["rowid"]))
            if item["type"]==3:
                #Time
                self.m.append(MenuTime(item["rowid"]))
            if item["type"]==4:
                #Text
                self.m.append(MenuText(item["rowid"]))
    def back(self):
        self.m.up()
    def left(self):
        self.m.left()
    def right(self):
        self.m.right()
    def select(self):
        if self.m[0].type==0:
            self.m.down()
    def home(self):
        self.m.home()
    def display(self):
        if self.m[0].type==0:
            try:
                line1 = " " + self.m[-1].display()
            except IndexError:
                line1 = " "
            try:
                line2 = "X" + self.m[0].display()
            except IndexError:
                line2 = " "
            try:
                line3 = " " + self.m[1].display()
            except IndexError:
                line3 = " "
            line4 = "Johnny :-)"
            return ([line1,line2,line3,line4] )
        else:
            return self.m[0].display()
class MenuItem:
    #Basic item of a menu. To be extended below.
    def __init__(self, name, itemtype):
        self.name=name
        self.type=itemtype
        self.content=False
    def display(self):
        return(self.name)
    def addcontent(self, content):
        self.content=content
    def escape(self):
        return True
    def back(self):
        return True

class MenuBinary(MenuItem):
    #Menuitem which can be either true or false
    def __init__(self, name, dbitem, dbase):
        self.name=name
        self.type=type
        self.selected=False
        self.dbitem=dbitem
        self.dbase = dbase
        super(MenuBinary, self).__init__(name, type)

    def select(self):
        self.dbwrite(self.selected)
    def left(self):
        self.selected=False
    def right(self):
        self.selected=True
    def dbwrite(self):
        self.dbase.writebinary(self, self.name, self.selected)
    def display(self):
        line1 = self.name
        line2 = "Yes"
        line3 = "N0" 
        if self.selected==True: line2.append(" X")
        else: line3.append(" X")
        return (line1, line2, line3)
        
        
class MenuList(MenuItem):
    #Menuitem which is a list from which the user selects
    def __init__(self, name, dbitem, dbase):
        self.name=name
        self.type=type
        self.selected=False
        self.dbitem=dbitem
        self.dbase = dbase
        super(MenuBinary, self).__init__(name, type)
    def select(self):
        self.dbwrite(self.selected)
    def left(self):
        if(self.selected==0):
            self.selected=self.total
        else:
            self.selected=self.selected-1
    def right(self):
        if(self.selected==0):
            self.selected=0
        else:
            self.selected=self.selected+1
    def dbwrite(self):
        self.dbase.writebinary(self, self.name, self.selected)

class MenuTime(MenuItem):
    #Menuitem where the user selects a time
    def __init__(self, name, dbitem, dbase):
        self.name=name
        self.type=type
        self.state=0
        #must find a way to convert ISO time to time object
        self.time=datetime.time(dbitem.data)
        self.selected=False
        self.dbitem=dbitem
        self.dbase=dbase
        super(MenuTime, self).__init__(name, type)
    def back(self):
        if(self.state==0):
            return True
        elif(self.state==1):
            self.state=0
            return False
        elif(self.state==2):
            self.state=1
            return False
    def select(self):
        if(self.state==0):
            self.state=1
        elif(self.state==1):
            self.state=2
        elif(self.state==2):
            self.dbwrite(self.selected)
            return True
    def left(self):
        if(self.state==0):
            if(self.time.hour==0):
                self.time.hour=23
            else:
                self.time.hour=self.time.hour-1
        elif(self.state==1):
            if(self.time.minute==0):
                self.time.minute=59
            else:
                self.time.minute=self.time.minute-1
        elif(self.state==2):
            if(self.time.second==0):
                self.time.second=59
            else:
                self.time.second=self.time.second-1
    def right(self):
        if(self.state==0):
            if(self.time.hour==23):
                self.time.hour=0
            else:
                self.time.hour=self.time.hour+1
        elif(self.state==1):
            if(self.time.minute==59):
                self.time.minute=0
            else:
                self.time.minute=self.time.minute+1
        elif(self.state==2):
            if(self.time.second==59):
                self.time.second=0
            else:
                self.time.second=self.time.second+1
    def dbwrite(self):
        self.dbitem.data=datetime.time.isoformat()
        self.dbase.write(self, self.dbitem)
    def display(self):
        line2 = str(self.hour)+":"+str(self.minute)+":"+str(self.second)
        if self.state==0:
            line1= str( self.time.hour + 1) + "      "
            line3= str( self.time.hour - 1) + "      "
        elif self.state==1:
            line1= "   " + str( self.time.minute + 1) + "   "
            line3= "   " + str( self.time.minute - 1) + "   "
        elif self.state==2:
            line1= "      " + str( self.time.second + 1)
            line3= "      " + str( self.time.second - 1)
        return (line1, line2, line3)
class MenuText(MenuItem):
    #Menuitem where the user types in some text
    def __init__(self, name, dbitem, length, text=None, chars=None):
        self.length=length
        if chars==None: self.chars=charlist("abcdefghijklmnopqrstuvwxyz0123456789.,:;?!-+/<>=#@$()")
        else: self.chars=chars
        if text==None: self.text=list('a')
        else: self.text=list(text)
        #Letter order is ascii 32,40-64,65-90,97-122
    def left(self):
        #set to one character "below" the current
        self.text[self.state]=self.chars.ante(self.text[self.state])
    def right(self):
        self.text[self.state]=self.chars.post(self.text[self.state])
    def select(self):
        if(self.state==self.length):
            self.dbwrite()
            return True
        elif(len(self.text)==self.state):
            self.text.append(self.text[self.state])
            self.state=self.state+1
    def dbwrite(self):
        self.dbitem.data = self.text
        self.dbase.write(self, self.dbitem)
    def display(self):
        if self.state<=7:
            line1= " "*self.state + self.chars.ante(self.text[self.state])
            line2= self.text[0:15]
            line3= " "*self.state + self.chars.post(self.text[self.state])
        else:
            line1= " "*8 + self.chars.ante(self.text[self.state])
            line2= self.text[self.state-8:self.state+8]
            line3= " "*8 + self.chars.post(self.text[self.state])
        return (line1, line2, line3)

def firstndeep(l, n):
    #Rob's cool function
    if n < 0:
        raise ValueError('expected a non-negative integer, received %n')
    elif n == 0:
        return l
    else:
        return firstndeep(l[0], n-1)

class Ring(list):
    def turn(self, amount):
        if amount != 0:
            super(Ring, self).__init__(self[amount:] + self[:amount])
    def __repr__(self):
        # To show the object is not a list.
        return 'Ring' + super(Ring, self).__repr__()

class Tree(UserList.UserList):
    def __init__(self, origin):
        self.data=[Ring([]), origin]
        self.cursor=0
        #Each node consist of a list of children and the element stored at that node
        #the cursor simply stores how deep we are in the list
    def append(self, x):
        #creates a new child of current cursor position
        # needs to go as deep as the current cursor
        if firstndeep(self.data, 2*self.cursor):
            firstndeep(self.data, 2*self.cursor+1).append([Ring([]),x])
    def down(self):
        self.cursor = self.cursor+1
    def up(self):
        if self.cursor > 0 :
            self.cursor = self.cursor-1
    def left(self):
        if firstndeep(self.data,2*self.cursor):
            firstndeep(self.data,2*self.cursor)[0].turn(-1)
    def right(self):
        if firstndeep(self.data,2*self.cursor):
            firstndeep(self.data,2*self.cursor)[0].turn(1)
    def home(self):
        self.cursor=0
    def __getitem__(self, i):
        try:
            return firstndeep(self.data, 2*self.cursor+1)[i][1]
        except IndexError:
            return False 
        #returns current element
class charlist(Ring):
    def ante(self, item):
        return self.data[self.index(item)-1]
    def post(self, item):
        return self.data[self.index(item)+1]

