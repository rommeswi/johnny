from time import sleep
import datetime
from string import ljust
from Adafruit_LED_Backpack.SevenSegment import SevenSegment
from RpiLcdBackpack import AdafruitLcd 
from Adafruit_LED_Backpack.HT16K33 import HT16K33_BLINK_OFF,\
    HT16K33_BLINK_HALFHZ, HT16K33_BLINK_1HZ, HT16K33_BLINK_2HZ


try:
    from gpiozero import LED,Button
except ImportError:
    print "Johnny could not find GPIOZERO"
class inputmonitor():
    def __init__(self):
        self.stopping = False
        self.blueled = LED(6)
        self.whiteled = LED(17)
        self.bluebutton = Button(5)
        self.whitebutton = Button(4)
        self.rotaryleft = Button(13)
        self.rotarypush = Button(19) 
        self.rotaryright = Button(26)
        self.lcd = AdafruitLcd()
        self.lcd.backlight(False)
        self.lcd.blink(False)
        self.lcd.cursor(False)
        self.lcd.clear()
        self.segment = SevenSegment(address=0x70)
        self.segment.begin()

    def reset(self):
        self.blueled = LED(6)
        self.whiteled = LED(17)
        self.bluebutton = Button(5)
        self.whitebutton = Button(4)
        self.rotaryleft = Button(13)
        self.rotarypush = Button(19) 
        self.rotaryright = Button(26)
        self.lcd = AdafruitLcd()
        self.lcd.backlight(False)
        self.lcd.blink(False)
        self.lcd.cursor(False)
        self.lcd.clear()
        self.segment = SevenSegment(address=0x70)
        self.segment.begin()
        
    def is_any_pressed(self):
        if(self.bluebutton.is_pressed | self.whitebutton.is_pressed | self.rotaryleft.is_pressed | self.rotaryright.is_pressed | self.rotarypush.is_pressed):
            return(True)
        else:
            return(False)
    def is_bluebutton_pressed(self):
        if(self.bluebutton.is_pressed):
            self.blueled.on()
            return(True)
        else:
            self.blueled.off()
            return(False)
    def is_whitebutton_pressed(self):
        if(self.whitebutton.is_pressed):
            self.whiteled.on()
            return(True)
        else:
            self.whiteled.off()
            return(False)
    def is_rotaryleft_pressed(self):
        if(self.rotaryleft.is_pressed):
            return(True)
        else:
            return(False)
    def is_rotaryright_pressed(self):
        if(self.rotaryright.is_pressed):
            return(True)
        else:
            return(False)
    def is_rotarypush_pressed(self):
        if(self.rotarypush.is_pressed):
            return(True)
        else:
            return(False)
    def wait_for_press(self):
        while self.is_any_pressed()==False:
            sleep(.01)
    def wait_for_press_return(self, n=0):
        t = datetime.datetime.now() + datetime.timedelta(seconds=n)
        if n==0:
            t = datetime.datetime.max
        while (t > datetime.datetime.now()) :
            if self.is_any_pressed()==True:
                if self.is_bluebutton_pressed()==True:
                    return "blue"
                elif self.is_whitebutton_pressed()==True:
                    return "white"
                elif self.is_rotaryleft_pressed()==True:
                    return "left"
                elif self.is_rotarypush_pressed()==True:
                    return "push"
                elif self.is_rotaryright_pressed()==True:
                    return "right"
                return(False)
            sleep(.001)
        return(False)

class outputmonitor():
    def __init__(self):
        self.lcd = AdafruitLcd()
        self.lcd.backlight(False)
        self.lcd.blink(False)
        self.lcd.cursor(False)
        self.lcd.clear()
        self.segment = SevenSegment(address=0x70)
        self.segment.begin()
        self.segment_brightness(7)

    def reset(self):
        self.lcd = AdafruitLcd()
        self.lcd.backlight(False)
        self.lcd.blink(False)
        self.lcd.cursor(False)
        self.lcd.clear()
        self.segment = SevenSegment(address=0x70)
        self.segment.begin()
        
    def lcd_lines(self,msg1,msg2,msg3,msg4):
        self.lcd.clear()
        if(self.lcd.backlightIsOn==False):
            self.lcd.backlight(True)
        self.lcd.message(ljust(msg1[:20],20)+ljust(msg3[:20],20)+"\n"+ljust(msg2[:20],20)+ljust(msg4[:20],20))
    def lcd_paragraph(self,msg):
        msg.replace('\n',' ')
        msg1 = msg[0:20]
        msg2 = msg[20:40]
        msg3 = msg[40:60]
        msg4 = msg[60:80]
        self.lcd_lines(msg1,msg2,msg3,msg4)
    def lcd_off(self):
        if(self.lcd.backlightIsOn==True):
            self.lcd.backlight(False)
        self.lcd.clear()
    def segment_show_number(self,t,h,z,e,c1=False,c2=True):
        self.segment.clear()
        self.segment.set_digit(0, t)
        self.segment.set_digit(1, h)
        self.segment.set_digit(2, z)
        self.segment.set_digit(3, e)
        self.segment.set_left_colon(c1)
        self.segment.set_colon(c2)
        self.segment.write_display()
    def segment_off(self):
        self.segment_brightness(0)
        self.segment.clear()
        self.segment.write_display()
    def segment_brightness(self, bright):
        self.segment.set_brightness(bright)
    def segment_blink(self, blink):
        if blink<=0:
            self.segment.set_blink(HT16K33_BLINK_OFF)
        elif blink<0.75:
            self.segment.set_blink(HT16K33_BLINK_HALFHZ)
        elif blink<1.5:
            self.segment.set_blink(HT16K33_BLINK_1HZ)
        else:
            self.segment.set_blink(HT16K33_BLINK_2HZ)
    def segment_update(self):
        self.segment.write_display()






